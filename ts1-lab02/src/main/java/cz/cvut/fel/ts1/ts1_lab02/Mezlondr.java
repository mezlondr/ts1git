package cz.cvut.fel.ts1.ts1_lab02;

public class Mezlondr {
	public long factorial(int n) {
		if(n < 0) {
			throw new UnsupportedOperationException("Not implemented for negative numbers.");
		}
		long result = 1L;
		for(long i = 1; i <= (long) n; ++i) {
			result *= i;
		}
		return result;
	}
	
	public long factorial2(int n) {
		if(n > 0) {
			return factorial2(n-1)*((long) n);
		}
		if(n == 0) {
			return 1L;
		}
		throw new UnsupportedOperationException("Not implemented for negative numbers.");
	}
}

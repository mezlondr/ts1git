package cz.cvut.fel.ts1.ts1_lab02;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;

public class MezlondrTest {
	@Test
	public void factorialTest() {
		final Mezlondr obj = new Mezlondr();
		assertEquals(1, obj.factorial2(0));
		assertEquals(1, obj.factorial(1));
		assertEquals(120, obj.factorial(5));
		assertThrows(UnsupportedOperationException.class, new Executable() {
			
			public void execute() throws Throwable {
				obj.factorial(-1);
			}
		});
	}
	
	@Test
	public void factorial2Test() {
		Mezlondr obj = new Mezlondr();
		assertEquals(1, obj.factorial2(0));
		assertEquals(1, obj.factorial2(1));
		assertEquals(120, obj.factorial2(5));
	}
}
